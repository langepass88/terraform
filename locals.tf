locals {
  droplet_ip = digitalocean_droplet.web[*].ipv4_address
  template_inv = templatefile("templates/inventory.tpl", { 
    ip_addrs = digitalocean_droplet.web[*].ipv4_address, 
    droplet_names = aws_route53_record.www[*].name,
    droplet_passwords = random_password.random_password[*].result})
}
