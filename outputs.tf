output "droplet_group" {
  value = {
    for index, droplet in digitalocean_droplet.web :
      droplet.name => {
        dns-name = aws_route53_record.www[index].fqdn
        ip = digitalocean_droplet.web[index].ipv4_address
        name_root = var.droplet_root
        password = nonsensitive(random_password.random_password[index].result)
      }
  }
}