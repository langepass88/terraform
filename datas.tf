data "digitalocean_ssh_key" "rebrain_ssh_pub_key" {
  name = var.rebrain_ssh_pub_key_name
}

data "aws_route53_zone" "rebrain_zone" {
  name = var.aws_hosted_zone_name
}