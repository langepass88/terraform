resource "digitalocean_ssh_key" "user_ssh_pub_key" {
  name = "user_public_ssh_key"
  public_key = file(var.user_ssh_pub_key_path)
}

resource "digitalocean_tag" "devops" {
  name = var.devops_tag
}

resource "digitalocean_tag" "user_email" {
  name = var.user_email_tag
}

resource "digitalocean_droplet" "web" {
  count = length(var.devs)
  image = "ubuntu-18-04-x64"
  name = "${var.devs[count.index]}"
  region = "nyc1"
  size = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.rebrain_ssh_pub_key.id, digitalocean_ssh_key.user_ssh_pub_key.fingerprint]
  tags = [digitalocean_tag.devops.id, digitalocean_tag.user_email.id]

  connection {
    type = "ssh"
    user = "root"
    agent = false
    private_key = file(var.user_ssh_private_key_path)
    host = self.ipv4_address
  }

  provisioner "remote-exec" {
    inline = [ 
      "/bin/echo \"${var.droplet_root}:${random_password.random_password[count.index].result}\" | /usr/sbin/chpasswd"
    ]
  }  
}

resource "random_password" "random_password" {
  count = length(var.devs)
  length = 16
  lower = true
  upper  = true
  special = false
}

resource "aws_route53_record" "www" {
  count = length(var.devs)
  zone_id = data.aws_route53_zone.rebrain_zone.zone_id
  name    = "${var.dns_name}-${var.devs[count.index]}"
  type    = "A"
  ttl     = 300
  records = [local.droplet_ip[count.index]]
}

resource "local_file" "inventory" {
    content  = local.template_inv
    filename = "${path.module}/inventory.txt"
}